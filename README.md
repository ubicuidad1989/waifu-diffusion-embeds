# waifu-diffusion-embeddings

## What is this

Stable Diffusion textual inversion embeddings, trained on Waifu Diffusion v1.2 (`wd-v1-2-full-ema [0b8c694b]` or pruned) or Waifu Diffusion v1.3. Results may vary.

## How to use with Webui

I recommend using [AUTOMATIC1111's stable-diffusion-webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui)

- Download the embeddings that you want in the `embeddings` directory
- Create `embeddings` folder in root directory of webui
- Paste the `.bin` or `.pt` files
- Use the token in your prompts
  - Example: `a close-up photo of a <marine>` with the `marine.bin` file

> It works with or without the `<diamond operator>`

You will get the best results when putting the token at the very end of the prompt. To quote the [Huggingface SD Concepts Library](https://huggingface.co/spaces/sd-concepts-library/stable-diffusion-conceptualizer):

>Prompting may not work as you are used to. objects may need the concept added at the end, styles may work better at the beginning.

# Available Embeddings WD 1.3

**in the wd-v1-3-embeddings folder**

## Kiriko (Overwatch 2)

Token: `<kiriko>`

[kiriko.pt](wd-v1-3-embeddings/kiriko.pt)

![](img/kiriko.png)
![](img/kiriko2.png)

> 50k steps, ~100 source images, BLIP Captioning

## Lucy (Cyberpunk Edgerunners)

Token: `<lucy-cyberpunk>`

![](img/lucy.png)
![](img/lucy2.png)
![](img/lucy3.png)

>`masterpiece, sitting in a cafe, holding a cup of coffee, a photo of lucy-cyberpunk, cyberpunk theme. Euler a, CFG 9.5, Steps: 20`

# Available Embeddings WD 1.2

## Houshou Marine (Hololive)

Token: `<marine>`

## Hoshimachi Suisei (Hololive)

Token: `<suisei-hololive>`

## Amane Kanata (Hololive)

Token: `<kanata-hololive>`

![](img/kanata2.png)

>`a cute photo of a <kanata-hololive>, Steps: 30, Sampler: Euler a, CFG scale: 7.5, Seed: 1341441461, Size: 512x512, Model hash: 0b8c694b`

[Also here's a different Kanata embedding](https://gitlab.com/16777216c/stable-diffusion-embeddings)

# How to do textual inversion yourself

**Just use the Textual Inversion tab in the webui**. Or [Follow this guide, either on Colab or locally](https://github.com/huggingface/diffusers/tree/main/examples/textual_inversion). You will need a GPU with at least ~~12GB~~8GB of VRAM.

**Feel free to open a pull request if you want to add your embeddings to this repo!**

> Textual Inversion Embeddings, like Model Weights, Hypernetworks, VAEs, and other Diffusion artifacts, are binary files in nature. THESE FILES CAN CONTAIN POTENTIALLY DANGEROUS ARBITRARY CODE. DO NOT USE RANDOM ML FILES DOWNLOADED FROM THE INTERNET WITHOUT VERIFYING THEIR CONTENTS.
> Webui's included safe-unpickle should protect you from most malicious files, but it's still a good idea to see what you're downloading.

# Moar 

## Botan (Hololive)

[viper1's Botan embeddingg](https://gitgud.io/viper1/stable-diffusion-embeddings)